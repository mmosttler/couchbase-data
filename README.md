### TODO:

- Update pom.xml following lines for this new service:
	<name>template-example</name>
	<description>template example project for 1800Flowers Spring Boot Service</description>

	<groupId>com.fd.servicenamespace.servicepkg</groupId>
	<artifactId>template-example</artifactId>


- Provide documentation describing this project and how it is used and any examples.


### Java SpringBoot Service template project

This project is based on a 1800Flowers Custom Project Template.

Improvements can be proposed via merge requests in the [original project](https://gitlab.com/-/ide/project/1800Flowers/custom-template-projects/springboot-service-starter-template).

### CI/CD with Custom Common Pipeline

This template provides the 18Flowers common pipeline for maven projects packaged in docker images and deployed to gke.
