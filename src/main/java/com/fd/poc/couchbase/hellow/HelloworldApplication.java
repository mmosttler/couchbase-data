package com.fd.poc.couchbase.hellow;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonArray;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.query.N1qlQuery;
import com.couchbase.client.java.query.N1qlQueryResult;
import com.couchbase.client.java.query.N1qlQueryRow;

public class HelloworldApplication 
{
	private static final Logger logger = LoggerFactory.getLogger(HelloworldApplication.class);
	
	public static void main(String... args) 
	{
        // Initialize the Connection
        Cluster cluster = CouchbaseCluster.create("localhost");
        cluster.authenticate("mydemo", "password");
        Bucket bucket = cluster.openBucket("mydemo");

        // Create a JSON Document
        JsonObject arthur = JsonObject.create()
            .put("name", "TIR")
            .put("email", "kingarthur@couchbase.com")
            .put("interests", JsonArray.from("Holy Grail", "African Swallows"));

        // Store the Document
        bucket.upsert(JsonDocument.create("u:sir_thur", arthur));

        // Load the Document and print it
        // Prints Content and Metadata of the stored Document
        if(logger.isInfoEnabled()) {
        	logger.info(bucket.get("u:sir_thur").toString());
        }

        // Create a N1QL Primary Index (but ignore if it exists)
        bucket.bucketManager().createN1qlPrimaryIndex(true, false);

        // Perform a N1QL Query
        N1qlQueryResult result = bucket.query(
            N1qlQuery.parameterized("SELECT name FROM `mydemo` WHERE $1 IN interests",
            JsonArray.from("African Swallows"))
        );

        // Print each found Row
        for (N1qlQueryRow row : result) {
            if(logger.isInfoEnabled()) {
            	logger.info(row.toString());
            }
        }
	}
}
