package com.fd.poc.couchbase.data.person.repository;

import java.util.List;

import org.springframework.data.couchbase.core.query.N1qlPrimaryIndexed;
import org.springframework.data.couchbase.core.query.Query;
import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;

import com.fd.poc.couchbase.data.person.Person;

@Repository
@N1qlPrimaryIndexed
//@ViewIndexed(designDoc = "person")
public interface PersonRepository extends CouchbaseRepository<Person, String> 
{
	List<Person> findByFirstName(String firstName);
	List<Person> findByLastName(String lastName);
	
    @Query("#{#n1ql.selectEntity} where #{#n1ql.filter} AND ANY phone IN phoneNumbers SATISFIES phone = $1 END")
    List<Person> findByPhoneNumber(String telephoneNumber);
    
    @Query("SELECT COUNT(*) AS count FROM #{#n1ql.bucket} WHERE #{#n1ql.filter} and lastName = $1")
    Long countPersonsByLastName(String lastName);

    @Query("SELECT COUNT(*) AS count FROM #{#n1ql.bucket}")
    Long countAllPersons();
}