package com.fd.poc.couchbase.data;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.config.AbstractCouchbaseConfiguration;
import org.springframework.data.couchbase.repository.config.EnableCouchbaseRepositories;

//@Configuration
//@EnableCouchbaseRepositories(basePackages = { "com.fd.poc.couchbase" })
public class MyCouchbaseConfig extends AbstractCouchbaseConfiguration 
{
//	@Value("${couchbase-bootstrap}")
	private String couchbaseBootstrap;
	
	@Override
	protected List<String> getBootstrapHosts() {
		return Arrays.asList(couchbaseBootstrap);
	}

	@Override
	protected String getBucketName() {
		return "mydemo";
	}

	@Override
	protected String getBucketPassword() {
		return "password";
	}
}