package com.fd.poc.couchbase.data.person;

import java.util.List;
import java.util.Optional;

public interface PersonService 
{
	Optional<Person> findOne(String id);

	List<Person> findAll();

	List<Person> findByFirstName(String firstName);
	
	List<Person> findByLastName(String lastName);

	List<Person> findByPhoneNumber(String telephoneNumber);
	
    long countPersons();

    Long countPersonsByLastName(String lastName);	
	
	void create(Person person);

	void update(Person person);

	void delete(Person person);
}
