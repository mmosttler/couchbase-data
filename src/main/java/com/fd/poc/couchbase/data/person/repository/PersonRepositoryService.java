package com.fd.poc.couchbase.data.person.repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.fd.poc.couchbase.data.person.Person;
import com.fd.poc.couchbase.data.person.PersonService;

@Primary
@Service
@Qualifier("PersonRepositoryService")
public class PersonRepositoryService implements PersonService 
{     
    @Autowired
    private PersonRepository repo; 
 
    public Optional<Person> findOne(String id) {
        return repo.findById(id);
    }
 
    public List<Person> findAll() {
        List<Person> people = new ArrayList<>();
        Iterator<Person> it = repo.findAll().iterator();
        while(it.hasNext()) {
            people.add(it.next());
        }
        return people;
    }
 
    public List<Person> findByFirstName(String firstName) {
        return repo.findByFirstName(firstName);
    }
 
    public List<Person> findByLastName(String lastName) {
        return repo.findByLastName(lastName);
    }
    
    public List<Person> findByPhoneNumber(String telephoneNumber)
    {
    	return repo.findByPhoneNumber(telephoneNumber);
    }
    
    public long countPersons() {
    	return repo.count();
    }
    
    public Long countPersonsByLastName(String lastName) {
    	return repo.countPersonsByLastName(lastName);
    }

    public void create(Person person) {
        person.setCreated(DateTime.now());
        repo.save(person);
    }
 
    public void update(Person person) {
        person.setUpdated(DateTime.now());
        repo.save(person);
    }
 
    public void delete(Person person) {
        repo.delete(person);
    }
}