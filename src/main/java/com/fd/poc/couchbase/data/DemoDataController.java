package com.fd.poc.couchbase.data;

import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fd.poc.couchbase.data.person.Person;
import com.fd.poc.couchbase.data.person.PersonService;

@RestController
@RequestMapping(value = "/demo", produces = "application/json")
public class DemoDataController 
{
	private static final Logger logger = LoggerFactory.getLogger(DemoDataController.class);
	
	@Autowired
	private PersonService personService;
	
	@GetMapping({"/persons"})
	public List<Person> getAllPersons()
	{
		logger.trace("ENTER: getAllPersons");
		List<Person> result = null;
	
		try
		{
			result = personService.findAll();			
		}
		catch(Exception e)
		{
			logger.error("Error getting all persons", e);
			throw e;
		}
		finally
		{
			logger.trace("EXIT: getAllPersons");			
		}
		return result;
	}

	@GetMapping(path={"/persons"}, params={"phoneNumber"})
	public List<Person> getAllPersonsByPhoneNumber(@RequestParam("phoneNumber") String phoneNumber)
	{
		logger.trace("ENTER: getAllPersonsByPhoneNumber");
		List<Person> result = null;
	
		try
		{
			result = personService.findByPhoneNumber(phoneNumber);			
		}
		catch(Exception e)
		{
			logger.error("Error getting all persons by phone number", e);
			throw e;
		}
		finally
		{
			logger.trace("EXIT: getAllPersonsByPhoneNumber");			
		}
		return result;
	}
	
	@GetMapping(path={"/persons"}, params={"firstName"})
	public List<Person> getPersonsByFirstName(@RequestParam("firstName") String firstName)
	{
		logger.trace("ENTER: getPersonsByFirstName");
		
		List<Person> result = null;
		
		try
		{
			result = personService.findByFirstName(firstName);
		}
		catch(Exception e)
		{
			logger.error("Error getting all persons by first name", e);
			throw e;
		}
		finally
		{
			logger.trace("EXIT: getPersonsByFirstName");			
		}
		
		return  result;
	}

	@GetMapping(path={"/persons"}, params={"lastName"})
	public List<Person> getPersonsByLastName(@RequestParam("lastName") String lastName)
	{
		logger.trace("ENTER: getPersonsByLastName");
		
		List<Person> result = null;
		
		try
		{
			result = personService.findByLastName(lastName);
		}
		catch(Exception e)
		{
			logger.error("Error getting all persons by last name", e);
			throw e;
		}
		finally
		{
			logger.trace("EXIT: getPersonsByLastName");			
		}
		
		return  result;
	}
	
	@GetMapping({"/persons/{id}"})
	public Person getPersonById(@PathVariable("id") String id)
	{
		logger.trace("ENTER: getPersonById");
		
		Person result = null;
		
		try
		{
			result = personService.findOne(id).orElseGet(Person::new);
		}
		catch(Exception e)
		{
			logger.error("Error getting all person by id", e);
			throw e;
		}
		finally
		{
			logger.trace("EXIT: getPersonById");			
		}
		
		return  result;
	}

	@GetMapping({"/persons/count"})
    public Long countPersons() 
    {
		logger.trace("ENTER: countPersons");
		
		Long result = NumberUtils.LONG_ZERO;
		
		try
		{
			result = personService.countPersons();
		}
		catch(Exception e)
		{
			logger.error("Error counting all persons", e);
			throw e;
		}
		finally
		{
			logger.trace("EXIT: countPersons");			
		}
		
		return  result;    	
    }

	@GetMapping(path={"/persons/count"}, params={"lastName"})    
    public Long countPersonsByLastName(@RequestParam("lastName") String lastName) 
    {    	
		logger.trace("ENTER: countPersonsByLastName");
		
		Long result = NumberUtils.LONG_ZERO;
		
		try
		{
			result = personService.countPersonsByLastName(lastName);
		}
		catch(Exception e)
		{
			logger.error("Error counting all persons by last name", e);
			throw e;
		}
		finally
		{
			logger.trace("EXIT: countPersonsByLastName");			
		}
		
		return  result;    	
    }
	
	@PostMapping({"/person"})
	public String addPerson(@RequestBody Person value)
	{
		logger.trace("ENTER: addPerson");

		personService.create(value);
		
		logger.trace("EXIT: addPerson");
		return "{ \"result\": \"SUCCESS\" }";
	}
}
